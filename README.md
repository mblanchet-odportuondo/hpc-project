# HPC Project

Ce projet a été réalisé par Mathieu Blanchet et Ortzi de Portuondo.

Le projet choisi est le Projet #5 "N-body simulation of gravitational interactions".

## Fichiers du projet
- `seq.py` : Le fichier avec le code original
- `seq_v2.py` : Ajout d'une seed, le temps d'exécution est calculé, les résultats sont vérifiés
- `seq_v3.py` : Le nombre de calculs a été reduit
- `seq_v4.py` : Appel de la fonction pythran
- `seq_pythran.py` : Fonction pythran
- `seq_v5.py` : Implémentation MPI
- `seq_v5.sh` : Fichier pour lancer seq_v5.py 

## Version 2
Dans cette version, nous avons rajouté une seed pour obtenir toujours les mêmes résultats pour pouvoir les comparer avec les autres versions.
Nous avons calculé le temps d'exécution de la fonction `animation.FuncAnimation` car c'est celle qui lance les calculs.
```
np.savetxt("resVec.txt",V) 
np.savetxt("resPart.txt",P)
```
Ces deux fonctions permettent d'exporter les résultats dans des fichiers textes pour pouvoir comparer et vérifier les résultats. Nous avons décidé de vérifier une précision de 0.000001.
On a remarqué que la fonction `update_velocities` est celle qui effectue les calculs, de plus, elle contient deux boucles imbriquées, ce qui peut être optimisé.

Les versions suivantes se basent sur cette version.

## Version 3 
Nous avons apperçu que certains calculs étaient effectués plusieurs fois, pour chaque particule on calcule l'influence de toutes les autres particules, ce qui fait qu'on calcule l'influence entre deux particules deux fois (le résultat possède un signe inverse).
Dans cette version, nous avons fait en sorte que les calculs ne se répètent pas, on passe de N² clsalcu à N²-N calculs. 

## Version 4
Dans cette version nous avons compilé la fonction `update_velocities` avec pythran, puis nous appellons la fonction pour effectuer les calculs.

## Version 5 
Dans cette version nous avons implémenté la fonction `update_velocities` avec MPI en partageant les calculs entre les processus. Nous avons partagé les calculs sur la première boucle, car chaque particules doit calculer l'influence par rapport à toutes les autres particules.

## Résultats

Voici les différents résultats obtenus pour chacunes des versions :

| Version | N = 10 | N = 100 | N = 1000 | N = 3000
| ------ | ------ | ------ | ------ | ------ |
|   Version initiale  (seq_v2)   |   0.0366s  | 0.0406s | 0.0456s | 0.0846s
|   Amélioration boucle for (seq_v3)   |  0.0362s  | 0.0407s | 0.0504s | 0.0768s
|   Version Pythran (seq_v4)   |  0.0367s   |  0.0389s | 0.0500s | 0.0772s
|   Version MPI (seq_v5)   |   0.0651s  | 0.0675s | 0,0728s | 0.11s

Ces résultats ont été obtenus en faisant une moyenne des différents temps d'exécution des programmes.
Globalement la meilleure version est celle qui utilise Pythran pour la compilation de la fonction `update_velocities`.

Toutefois, on constate que les résultats ne sont pas significativement meilleurs comparés à la version initiale.
