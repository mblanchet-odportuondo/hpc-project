import numpy as np
import matplotlib.pyplot as plt
import time

from matplotlib import animation, rc
from IPython.display import HTML, Image



fig, ax = plt.subplots()

a=20
ax.set_xlim(( -a, a))
ax.set_ylim(( -a, a))

#line = ax.scatter([], [])
#qv = ax.quiver([], [], [], [])
temps_total=0
dt=1e-2
Vmax=20.
N = 1000
P = np.zeros((N, 2))
V = np.zeros((N, 2))
np.random.seed(1234)
r = np.random.random((N,1))
P[:,0:1] = np.cos(2*np.pi*r)
P[:,1:2] = np.sin(2*np.pi*r)
r = np.random.random((N,1))
P *= np.sqrt(-2*np.log(r+1e-5))*4

r2 = P[:,0]**2+P[:,1]**2
C=Vmax/(1+r2)
V[:,0]=C*P[:,1]
V[:,1]=-C*P[:,0]


line = ax.scatter(P[:,0], P[:,1])
qv = ax.quiver(P[:,0], P[:,1], V[:,0], V[:,1])

def update_fig(P,V):
    line.set_offsets(P)
    qv.set_offsets(P)
    qv.set_UVC(V[:,0], V[:,1])

def C_LJ(s):
    M=1
    eps2=1e-6
    return M/(eps2+s)**1.5

def update_velocities(P,V,alpha):
    C= np.empty((N,N))
    for i in range(N):
        for j in range(N):
            d = P[j,:] - P[i,:]
            if(i<j):
                r2 = d[0]**2 + d[1]**2
                C[i,j]=alpha*C_LJ(r2)
                V[i,:] += C[i,j]*d
            else:
                V[i,:] += C[j,i]*d

def update_parts(P,V):
    update_velocities(P,V, 0.5*dt)
    P[...] += dt*V
    update_velocities(P,V, 0.5*dt)

def init():
    update_fig(P,V)
    return (line,)

def animate(i):
    update_parts(P,V)
    update_fig(P,V)
    return (line,)

t0 = time.time()
anim = animation.FuncAnimation(fig, animate, init_func=init,
                               frames=20, interval=20, blit=True)
print("Temps : ", time.time() - t0)
#anim.save('output.gif', writer='imagemagick', fps=20)


#np.savetxt("resVec.txt",V)
#np.savetxt("resPart.txt",P)


if(((np.absolute(V - np.loadtxt("resVec.txt")) < 0.000001).all()) and (np.absolute(P - np.loadtxt("resPart.txt")) < 0.000001).all()):
    print("Les résultats sont corrects")
else:
    print("Les résultats sont incorrects")
