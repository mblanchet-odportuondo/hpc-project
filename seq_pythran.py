import numpy as np

def C_LJ(s):
    M=1
    eps2=1e-6
    return M/(eps2+s)**1.5

#pythran export update_velocities(float64[][], float64[][], float)
def update_velocities(P,V,alpha):
    N=P.shape[0]
    for i in range(N):
       for j in range(N):
           d = P[j,:] - P[i,:]
           r2 = d[0]**2 + d[1]**2
           C=alpha*C_LJ(r2)
           V[i,:] += C*d


